package me.lusu07.AutoNick.MySQL;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.ConsoleCommandSender;

import java.sql.*;

/**
 * Created by Lukas on 26.03.2015.
 */
public class MySQL {

    public static String host;
    public static String port;
    public static String database;
    public static String username;
    public static String password;
    public static Connection con;

    static ConsoleCommandSender console = Bukkit.getConsoleSender();

    //Connect to MySQL
    public static void connect() {
        if(!isConnected()) {
            try {
                console.sendMessage(" ");
                console.sendMessage(ChatColor.GREEN + "Initialisiere MySQL Backend...");
                con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
                System.out.println(ChatColor.GREEN + "MySQL Verbindung aufgebaut!");
            } catch (SQLException e) {
                System.out.println("Verbindung konnte nicht hergestellt werden!");
                System.out.println("SQLException: " + e.getMessage());
                System.out.println("SQLState: " + e.getSQLState());
                System.out.println("VendorError: " + e.getErrorCode());
            }
        }
    }

    //Disconnect MySQL
    public static void disconnect() {
        if(isConnected()) {
            try {
                con.close();
                System.out.println("MySQL Verbindung geschlossen!");
            } catch (SQLException e) {
                System.out.println("Verbindung konnte nicht geschlossen werden!");
                System.out.println("SQLException: " + e.getMessage());
                System.out.println("SQLState: " + e.getSQLState());
                System.out.println("VendorError: " + e.getErrorCode());
            }
        }
    }

    //Returns Connection
    public static boolean isConnected() {
        return (con == null ? false : true);
    }

    //Process a Statement
    public static void update(String qry) {
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(qry);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Returns a Result
    public static ResultSet getResult(String qry) {
        try {
            PreparedStatement ps = con.prepareStatement(qry);
            return ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void createData(String player) {
        OfflinePlayer pl = Bukkit.getOfflinePlayer(player);
        String uuid = pl.getUniqueId().toString();

        ResultSet rs = getResult("SELECT uuid FROM autonick WHERE uuid = '" + uuid + "'"); //TODO: database?

        try {
            if(!rs.next()) {
                update("INSERT INTO autonick VALUES('" + uuid + "', false)");
                return;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean isInDataBase(OfflinePlayer target) {
        String uuid = target.getUniqueId().toString();

        boolean isInDatabase = false;

        ResultSet rs = getResult("SELECT uuid FROM autonick WHERE uuid = '" + uuid + "'");

        try {
            if(rs.next()) {
                isInDatabase = true;
            } else {
                isInDatabase = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Boolean.valueOf(isInDatabase).booleanValue();
    }

    public static void setNicked(String player, boolean nicked) {
        OfflinePlayer pl = Bukkit.getOfflinePlayer(player);
        String uuid = pl.getUniqueId().toString();

        update("UPDATE ");
    }

    //public static boolean isNicked(String player) {
//
  //  }
}
