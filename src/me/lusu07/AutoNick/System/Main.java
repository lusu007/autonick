package me.lusu07.AutoNick.System;

import me.lusu07.AutoNick.MySQL.MySQLFile;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Lukas on 15.02.2015.
 */
public class Main extends JavaPlugin {

    public void onEnable() {
        //Erstellt MySQl Cfg
        MySQLFile file = new MySQLFile();
        file.setStandard();
        file.readData();
    }

    @Override
    public void onDisable() {

    }
}
